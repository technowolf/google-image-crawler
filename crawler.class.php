<?php

class GoogleCrawler
{

    // Parameters
    private $searchString = '';
    private $googleRealURL = '';
    private $htmlimages = '';
    private $error = '';
    private $counter = 0;

    private $googleStringConfig = array('safe' => 'search', 'cr' => '', 'tbs' => '');
    private $config = array(
        'imagesLimit' => 1,
        'thumbWidth' => 0,
        'thumbHeight' => 300,
        'savePath' => '/images/crawler',
        'savePathThumbs' => '/images/crawler'
    );

    private $modes = array();
    private $urls = array();
    private $debug = array();


    function curl($url, $retry = 0)
    {
        if ($retry > 5) {
            print "max retries\n";
            return "loop!";
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux i686; rv:20.0) Gecko/20121230 Firefox/20.0');
        curl_setopt($ch, CURLOPT_HEADER, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_REFERER, 'http://www.google.com/');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);

        if (preg_match("|Location: (https?://\S+)|", $result, $m)) {
            print "doing redirection!\n$m[1]\n";
            return curl($m[1], "Mozilla/5.0 (X11; Linux i686; rv:20.0) Gecko/20121230 Firefox/20.0", $retry + 1);
        }


        return $result;
    }

    protected function core()
    {

        $googleStringConfig = http_build_query($this->googleStringConfig);
        $googleStringConfig = str_replace("%3A", ":", $googleStringConfig);
        $googleStringConfig = str_replace("%2C", ",", $googleStringConfig);
        $this->searchString = str_replace(" ", "+", $this->searchString);
        $this->googleRealURL = "https://www.google.com/search?q=" . $this->searchString . "&tbm=isch&safe=search&cr=country&tbs=sur:,ift:color,isz:,ic:color,ctr:country&gws_rd=cr" . $googleStringConfig;
        $google = $this->curl($this->googleRealURL);

        if (isset($this->config['imagesLimit']) && $this->config['imagesLimit'] < 100) {
            $limit = $this->config['imagesLimit'];
        } else {
            $limit = 10;
        }

        $images = 0;
        while (strpos($google, 'notranslate">') !== false && $images < $limit) {
            $pos1 = strpos($google, 'notranslate">') + 13;
            $google = substr($google, $pos1, strlen($google));
            $pos2 = strpos($google, '</div>');
            $js = json_decode(substr($google, 0, $pos2));

            $el = array();
            $el['ou'] = stripslashes($js->ou);
            $el['ow'] = $js->ow;
            $el['oh'] = $js->oh;
            $final[] = $el;

            $images++;
        }
        if ($this->modes['debug'] == 1) {
            $this->debug[] = str_replace("<", "&lt;", $google);
        }
        $this->urls[$this->searchString] = $final;
    }

    protected function adjustTBS()
    {
        $newTBS = array();
        $keys = array();
        $TBSString = $this->googleStringConfig['tbs'];
        $parts = explode(",", $TBSString);
        for ($i = count($parts) - 1; $i >= 0; $i--) {
            if (trim($parts[$i]) == '') {
                continue;
            }
            $var = substr($parts[$i], 0, strpos($parts[$i], ":"));
            if (!in_array($var, $keys)) {
                $newTBS[] = $parts[$i];
                $keys[] = $var;
            }
        }

        $this->googleStringConfig['tbs'] = implode(",", $newTBS);
    }

    protected function makeDirectory($dirPath, $mode = 0777)
    {
        return is_dir($dirPath) || mkdir($dirPath, $mode, true);
    }

    protected function error($id_err)
    {
        $this->error = $id_err;
        exit();
    }

    protected function show()
    {

        // Populate the SHOW Variable
        if (count($this->urls) == 0) {
            $this->error('ERR_SHOW_NO_RESULTS');
        }

        // Generate and Save Visual Results

        $outText = '';
        foreach ($this->urls as $key => $results) {
            $outText .= "<b>Search: '" . $key . "'</b><br/><div>";
            foreach ($results as $result) {

                $ext = substr($result['ou'], strrpos($result['ou'], '.') + 1, 4);
                switch ($ext) {
                    case 'jpg':
                        $coverimg = @imagecreatefromjpeg($result['ou']);
                        break;
                    case 'png':
                        $coverimg = @imagecreatefrompng($result['ou']);
                        break;
                    case 'gif':
                        $coverimg = @imagecreatefromgif($result['ou']);
                        break;
                }

                if (!$coverimg) {
                    continue;
                } // If download failed, go for the next one!


                $outText .= "<span><a href='" . $result['ou'] . "' title='" . $result['ou'] . " - Click to Enlarge *_-' target='_blank'><img src='" . $result['ou'] . "' /></a></span>";
            }
            $outText .= "</div>";
        }

        print $outText;

    }

    protected function saveFoundImages($urlsArray = '', $thumbs = 0)
    {
        if (!is_array($urlsArray)) {
            if (trim($urlsArray) == '') {
                $this->error("ERR_SAVEIMAGES_NO_URLS");
            }
            $urlsArray = array($urlsArray);
        }

        $fullPath = $_SERVER['DOCUMENT_ROOT'] . $this->config['savePath'];
        $this->makeDirectory($fullPath);

        $counter = 1;
        $name = str_replace(" ", "_", strtolower($this->searchString));
        $name = str_replace("+", "_", strtolower($name));
        if ($name == '') {
            $name = 'image' . $this->counter;
        }
        foreach ($urlsArray as $result) {
            $ext = substr($result['ou'], strrpos($result['ou'], '.') + 1, 4);

            switch ($ext) {
                case 'jpg':
                    $coverimg = @imagecreatefromjpeg($result['ou']);
                    break;
                case 'png':
                    $coverimg = @imagecreatefrompng($result['ou']);
                    break;
                case 'gif':
                    $coverimg = @imagecreatefromgif($result['ou']);
                    break;
            }

            if (!$coverimg) {
                continue;
            } // If download failed, go for the next one!

            file_put_contents($fullPath . "/" . $name . "_" . $counter . "." . $ext, file_get_contents($result['ou']));
            $counter++;
        }

        if ($thumbs == 1) {
            $fullPathThumbs = $_SERVER['DOCUMENT_ROOT'] . $this->config['savePathThumbs'];
            $this->makeDirectory($fullPathThumbs);

            $counter = 1;
            $name = str_replace(" ", "_", strtolower($this->searchString));
            $name = str_replace("+", "_", strtolower($name));
            if ($name == '') {
                $name = 'image' . $this->counter;
            }
            foreach ($urlsArray as $result) {
                $ext = substr($result['ou'], strrpos($result['ou'], '.') + 1, 4);
                $destpath = $fullPathThumbs . "/" . $name . "_" . $counter . "." . $ext;

                switch ($ext) {
                    case 'jpg':
                        $coverimg = @imagecreatefromjpeg($result['ou']);
                        break;
                    case 'png':
                        $coverimg = @imagecreatefrompng($result['ou']);
                        break;
                    case 'gif':
                        $coverimg = @imagecreatefromgif($result['ou']);
                        break;
                }

                if (!$coverimg) {
                    continue;
                } // If download failed, go for the next one!

                list($width, $height) = getimagesize($result['ou']);

                if ($this->config['thumbWidth'] > 0 && $this->config['thumbHeight'] == 0) {
                    $new_width = $this->config['thumbWidth'];
                    $new_height = ($width / $height) * $this->config['thumbWidth'];
                }
                if ($this->config['thumbWidth'] == 0 && $this->config['thumbHeight'] > 0) {
                    $new_height = $this->config['thumbHeight'];
                    $new_width = ($width / $height) * $this->config['thumbHeight'];
                }
                if ($this->config['thumbWidth'] == 0 && $this->config['thumbHeight'] == 0) {
                    $new_height = 300;
                    $new_width = ($width / $height) * 300;
                }
                if ($this->config['thumbWidth'] > 0 && $this->config['thumbHeight'] > 0) {
                    $new_width = $this->config['thumbWidth'];
                    $new_height = $this->config['thumbHeight'];
                }

                $image_fin = imagecreatetruecolor($new_width, $new_height);
                imagecopyresampled($image_fin, $coverimg, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

                switch ($ext) {
                    case 'jpg':
                        $coverimg = imagejpeg($image_fin, $destpath, 70);
                        break;
                    case 'png':
                        $coverimg = imagepng($image_fin, $destpath, 8);
                        break;
                    case 'gif':
                        $coverimg = imagegif($image_fin, $destpath, 70);
                        break;
                }

                $counter++;

            }
        }
        $this->counter++;

    }


    function set($configArray)
    {
        if (!is_array($configArray) || count($configArray) == 0) {
            return false;
        } // Quit Without Setting Config
        foreach ($configArray as $key => $value) {

            // Set config
            $this->config[$key] = $value;

            // Special Settings for Google String
            switch ($key) {
                case 'googleString':
                    $this->googleStringConfig = explode("&", $value);
                    unset($this->googleStringConfig['q']);
                    unset($this->googleStringConfig['as_q']);
                    break;
                case 'safeSearch':
                    if ($key == 1) {
                        $this->googleStringConfig['safe'] = 'active';
                    } else {
                        $this->googleStringConfig['safe'] = 'search';
                    }
                    break;
                case 'searchColor':
                    if (!in_array($value, array('color', 'graytones', 'trans'), true)) {
                        $this->googleStringConfig['tbs'] = $this->googleStringConfig['tbs'] . ',ic:specific,isc:' . $value;
                        $this->adjustTBS();
                    } else {
                        if ($value == 'graytones') {
                            $value == 'gray';
                        }
                        $this->googleStringConfig['tbs'] = $this->googleStringConfig['tbs'] . ',ic:' . $value;
                        $this->adjustTBS();
                    }
                    break;
                case 'searchSize':
                    if (strlen($value) <= 1) {
                        $this->googleStringConfig['tbs'] = $this->googleStringConfig['tbs'] . ',isz:' . $value;
                    } else {
                        $this->googleStringConfig['tbs'] = $this->googleStringConfig['tbs'] . ',isz:lt,islt:' . $value;
                    }
                    $this->adjustTBS();
                    break;
                case 'searchFileType':
                    $this->googleStringConfig['tbs'] = $this->googleStringConfig['tbs'] . ',ift:' . strtolower($value);
                    $this->adjustTBS();
                    break;
                case 'searchCountry':
                    $this->googleStringConfig['tbs'] = $this->googleStringConfig['tbs'] . ',ctr:country' . strtoupper($value);
                    $this->adjustTBS();
                    $this->googleStringConfig['cr'] = 'country' . strtoupper($value);
                    break;
                case 'searchCopyright':
                    $this->googleStringConfig['tbs'] = $this->googleStringConfig['tbs'] . ',sur:' . strtolower($value);
                    $this->adjustTBS();
                    break;
            }

        }
    }

    function search($keysArray, $modes = '', $config = '')
    {
        if ($modes == '') {
            $modes = array("show" => 0, "save" => 0, "thumbs" => 0, "debug" => 0);
        }
        if (!is_array($keysArray)) {
            if (trim($keysArray) == '') {
                $this->error("ERR_SEARCH_EMPTY_SEARCHSTRING");
            }
            $keysArray = array($keysArray);
        }
        $this->modes = $modes; // Save Modes
        $this->set($config); // Save Config

        foreach ($keysArray as $key) {
            $this->searchString = $key;
            $this->core();
            if ($this->modes['save'] == 1) {
                $this->saveFoundImages($this->urls[$key], $this->modes['thumbs']);
            } // We save images after every search to avoid flooding google with requests
            sleep(2);
        }
        if ($this->modes['show'] == 1) {
            $this->show();
        }
        if ($this->modes['debug'] == 1) {
            print "<br/><br/><b>DEBUG SEARCH</b><br/><br/>";
            print_r($this->debug);
        }
        return $this->urls;
    }

    function save($keysArray, $modes, $config = '')
    {
        if (!is_array($keysArray)) {
            if (trim($keysArray) == '') {
                $this->error("ERR_SEARCH_EMPTY_URLSTRING");
            }
            $keysArray = array($keysArray);
        }
        $this->modes = $modes;
        $this->set($config);
        foreach ($keysArray as $key) {
            $this->saveFoundImages($key, $this->modes['thumbs']);
        }
        return 1;
    }

}

?>